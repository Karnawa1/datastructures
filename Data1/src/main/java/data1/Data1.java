

package data1;


public class Data1 {

    public static void main(String[] args) {
        List list = new List(3);
        list.add(1, 0);
        list.add(2, 1);
        list.add(3, 2);
        list.add(4, 1);
        System.out.println("Size: " + list.size());
        System.out.println("Element at index 0: " + list.get(0));
        System.out.println("Element at index 1: " + list.get(1));
        System.out.println("Element at index 2: " + list.get(2));
        System.out.println("Element at index 3: " + list.get(3));
        list.delete(1);
        System.out.println("Size: " + list.size());
        System.out.println("Element at index 1: " + list.get(1));
        list.replace(5, 2);
        System.out.println("Element at index 2: " + list.get(2));
    }
}
