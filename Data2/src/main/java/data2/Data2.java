
package data2;

public class Data2 {

    public static void main(String[] args) {
        LinkedList list = new LinkedList();
        
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        
        System.out.println("Original list:");
        list.printList();
        
        list.addAtIndex(2, 5);
        System.out.println("After adding an element at index 2:");
        list.printList();
        
        list.delete(3);
        System.out.println("After deleting an element 3:");
        list.printList();
        
        list.replace(1, 6);
        System.out.println("After replacing the element at index 1:");
        list.printList();
    }
}
